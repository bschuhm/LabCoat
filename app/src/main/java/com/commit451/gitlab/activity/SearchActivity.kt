package com.commit451.gitlab.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import com.commit451.gitlab.R
import com.commit451.gitlab.adapter.SearchPagerAdapter
import com.commit451.gitlab.databinding.ActivitySearchBinding
import com.commit451.jounce.Debouncer
import com.commit451.teleprinter.Teleprinter
import timber.log.Timber

/**
 * Search for :allthethings:
 */
class SearchActivity : BaseActivity() {

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, SearchActivity::class.java)
        }
    }

    private lateinit var binding: ActivitySearchBinding
    private lateinit var adapterSearch: SearchPagerAdapter
    private lateinit var teleprinter: Teleprinter

    private val debouncer = object : Debouncer<CharSequence>() {
        override fun onValueSet(value: CharSequence) {
            search()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        teleprinter = Teleprinter(this)
        binding.toolbar.setNavigationIcon(R.drawable.ic_back_24dp)
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        adapterSearch = SearchPagerAdapter(this, supportFragmentManager)
        binding.viewPager.adapter = adapterSearch
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.textSearch.requestFocus()
        binding.buttonClear.setOnClickListener {
            binding.buttonClear.animate().alpha(0.0f)
                .withEndAction { binding.buttonClear.visibility = View.GONE }
            binding.textSearch.text.clear()
            teleprinter.showKeyboard(binding.textSearch)
            debouncer.cancel()
        }
        binding.textSearch.setOnEditorActionListener { _, _, _ ->
            if (binding.textSearch.text.isNullOrEmpty()) {
                binding.textSearch.setText("labcoat")
            }
            search()
            teleprinter.hideKeyboard()
            false
        }
        binding.textSearch.addTextChangedListener(onTextChanged = { s, _, before, count ->
            if (s.isNullOrEmpty()) {
                binding.buttonClear.animate().alpha(0.0f)
                    .withEndAction { binding.buttonClear.visibility = View.GONE }
            } else if (count == 1) {
                binding.buttonClear.visibility = View.VISIBLE
                binding.buttonClear.animate().alpha(1.0f)
            }
            if (s != null && s.length > 3) {
                Timber.d("Posting new future search")
                debouncer.value = s
            }
            //This means they are backspacing
            if (before > count) {
                Timber.d("Removing future search")
                debouncer.cancel()
            }
        }
        )
    }

    private fun search() {
        val query = binding.textSearch.text.toString()
        Timber.d("Searching $query")
        adapterSearch.searchQuery(query)
    }
}
